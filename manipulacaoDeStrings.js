/* 
    EXERCÍCIO DO TREINAMENTO DA INJUNIOR - AULA DE JS
    O programa recebe uma string qualquer e imprime na tela a posição no alfabeto de cada letra da string em questão.
    Os "." são representados pelo 0, "," pelo -1 e os espaços em branco são impressos normalmente
*/

// Declaração da string inicial
var string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum. Phasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit."

// Transformando a string em uma lista com todos os caracteres maiúsculos.
// Tornar todas as letras maiúsculas é necessário para fazer a comparação com o charCode
// mais tarde no código, que é o que facilita o processo de saber a posição de cada letra no
// alfabeto sem ter que fazer um if específico pra cada uma
var listedString = Array.from(string.toUpperCase())

// Declaração da string final e iteração do array com os caracteres
var finalString = ""
listedString.forEach(char => {
    
    let charCode = char.charCodeAt(0)   // Armazena o valor do char code do caracter atual na variável charCode
    
    // Esse trecho de código verifica primeiro se o caracter atual é uma letra (se tem o char code entre
    // 65 e 90, que são todas as letras maiúsculas), se isso for verdadeiro ele adiciona a posição da letra no
    // alfabeto (o valor do char code dela menos 64) e um espaço em branco para facilitar a leitura.
    // Depois disso é verificado se o caracter em questão é um ponto ou vírgula, adicionando o valor 0 ou -1,
    // respectivamente, na string final.
    // Se nenhum desses casos forem verdadeiros, então o caracter em questão é um espaço em branco que é também
    // adicionado na string final.
    if (charCode >= 65 && charCode <= 90) {
        finalString += (charCode -  64) + " "
    } else if (char === '.') {
        finalString += 0 + " "
    } else if (char === ',') {
        finalString += -1 + " "
    } else {
        finalString += char
    }

})

// Imprime a string final com os valores das letras no alfabeto
console.log(finalString)